# CPlusHomework_03
Create a class for storing the calendar dates. Provide the possibility to work with dates in different formats (e.g. DD.MM.YYYY, DD-MM-YYYY, YYYY.MM.DD) and modify the date with the specified number of dates. Also you need to overload following operators:
* operator- (for finding the date differences and comparison)
* operator<<
* operator>>

Don't use standard date types & functions

