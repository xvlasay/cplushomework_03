#include <iostream>
#include "my_date.h"
#include <string>
#include <sstream> 
using namespace std;
int main() {
	char str1[] = "12/12/2018";
	my_date dt;
	dt.showDate();
	dt.setDate(str1);
	dt.setDate(str1);
	dt.showDate();
	strcpy(str1,"12.12.2018");
        dt.setDate(str1);
	dt.showDate();
	strcpy(str1,"12.30.2018");
        dt.setDate(str1);
	dt.showDate();
	strcpy(str1,"2018.20.12");
        dt.setDate(str1);
	dt.showDate();
	cout << "cdc = " << dt.calc_cdc(2018) << endl;
 	int diff;
	my_date df;
	char str2[] = "10/02/2018";
	df.setDate(str2);
	cout << "First date:" << endl;
	cout << dt;
	cout << "Second date:" << endl;
	cout << df;	
	diff = dt - df;
	cout << "diff = " << diff << endl;
	cout << "overloading >>  operator: \"20.12.2018\" >> dt " << endl;
	"20.12.2018" >> dt;
	cout << dt;
	cout << "overloading plus operator: \"20.12.2018\" + 31 days:" << endl;
        dt + 31;
        cout << dt << endl;
	"20.01.2019" >> dt;
	cout << "overloading minus-operator: \"20.01.2019\" - 40 days:" << endl;
	dt - 40;
        cout << dt << endl;
//	dt.showDate();
	return 1;
}
