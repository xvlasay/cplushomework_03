#include <iostream>
#include <string> 
#include <regex> 
#include <iomanip> 
//#include <vector>
using namespace std;
constexpr int day_month[] =  { 31,28,31,30,31,30,31,31,30,31,30,31 } ;

class my_date 
{ 
private:
	int month; 
	int day; 
	int year; 
	int cdc;
	std::string line;

public: 
	my_date(): month(1),day(1),year(2018),cdc(0),line("01/01/2018")  {}; 
	void showDate(); 
	int setDate(const char *);
	int calc_cdc (int); 
	friend int friend_cdc ( my_date&, int);
	int operator-( my_date& d) {
		int cdc1 = 0;
		int cdc2 = 0;
		int cdc_y = 0;
		if (year > d.year) cdc_y = year;
		else cdc_y = d.year;
		cdc1 = calc_cdc(cdc_y);
		cdc2 = friend_cdc(d,cdc_y);
		//cout << "cdc1: " << cdc1 << endl;
		//cout << "cdc2: " << cdc2 << endl;
		return (cdc1 - cdc2);
	}	
// Unary "plus-operator": increments specified number of days to itself.
	void  operator+(int add_days) {
		int delta_days = day_month[month -1] - day; 
		int cur_day = day;
		int cur_month = month;
		int cur_year = year;
		while (add_days > 0) {
			if (add_days > delta_days) {
				add_days = add_days - delta_days;	
				cur_day = 1;
				cur_month++;		
				if (cur_month > 12) {
					cur_month = 1;
					cur_year++;
				}
				delta_days = day_month[cur_month -1];
				
			} else {
				day = cur_day + add_days; 
				month = cur_month;
				year = cur_year;
				add_days = 0;
				break;
			} 
		}
	        std::stringstream ss;
       		ss << std::setfill('0') << std::setw(2) << day << "/" << std::setw(2)
		 << month << "/" << std::setw(4) << year;
        	line = ss.str();
		//cout << "new line: "  << line << endl;
		return;
	}
// Unary "minus-operator": decrements specified number of days from itself.
 void  operator-(int minus_days) {
                int delta_days =  day;
                int cur_day = day;
                int cur_month = month;
                int cur_year = year;
                while (minus_days > 0) {
                        if (minus_days > delta_days) {
                                cur_month--;
                                if (cur_month < 1) {
                                        cur_month = 12;
                                        cur_year--;
                                }
                                minus_days = minus_days - delta_days;
				cur_day = day_month[cur_month - 1];
                                delta_days = day_month[cur_month -1];

                        } else {
                                day = cur_day - minus_days;
                                month = cur_month;
                                year = cur_year;
                                minus_days = 0;
                                break;
                        }
                }
                std::stringstream ss;
		ss << std::setfill('0') << std::setw(2) << day << "/" << std::setw(2) <<
		month << "/" << std::setw(4) << year;
                line = ss.str();
                //cout << "new line: "  << line << endl;
                return;
	}
//
	friend std::ostream& operator<<( std::ostream& output, const my_date  &d ) {
         output <<  d.line << endl;
         return output;
        }
	friend  void operator>>(string input, my_date &d ) { 
		const char* str = input.c_str();
		d.setDate(str);
         	return ;            
	}

	
}; 

void my_date::showDate() {
cout << "day/month/year: " << std::setfill('0') << std::setw(2) << day << "/" << std::setw(2) << month << "/" << std::setw(4) << year << endl;
	return;
}
int my_date::setDate(const char*  str1) { 
	int rcode = 0;
	regex b("(^([0][1-9]|[1-2][0-9]|[3-3][0-1])[/|.|-]([0-0][0-9]|[1-1][0-2])[/|.|-](19|20)[0-9]{2})");
	if ( regex_match(str1, b) ) {
       		cout << "String : " << str1 << " matches regular expression  \n"; 
		rcode = 1;
		line = std::string(str1);
		cout << "line = " << line << endl;
		day = stoi((line.substr(0,2)));
		//cout << "day: " << day << endl;
		month = stoi((line.substr(3,2)));
                //cout << "month: " << month << endl;
		year = stoi((line.substr(6,4)));
                //cout << "year: " << year << endl;
	}
	else {
		cout << "String : " << str1 << " doesn't match regular expression  \n"; 
		rcode = 0;
	}
	return rcode;
}
int my_date::calc_cdc(int yyyy) {
	int rc = 0;
	int end_month = 0;
	if (yyyy > year) {
		cdc = 0;
		return 0;
	}
	for (int i = yyyy; i <= year; i++) {
		if ( i == year) end_month = month - 1; 
		else end_month = 11;
		for (int j = 0; j < end_month - 1; j++) {
			rc = rc + day_month[j];			
		}
		if ( i == year) rc = rc + day;
	}
	cdc = rc;
	return rc;
}
int friend_cdc ( my_date& obj, int yyyy) {
	int rc = obj.calc_cdc(yyyy);
	return rc; 
}
